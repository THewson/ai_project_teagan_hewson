#pragma once
#include <cmath>

class Vector2
{
public:
	Vector2();
	Vector2(float x, float y);

	~Vector2();

	////////////////////////////
	//    Basic operations    //
	////////////////////////////
	
	// Addition
	Vector2 add(Vector2 & a_second);
	Vector2 operator + (Vector2 & a_second) { return add(a_second); }
	Vector2 operator += (Vector2 & a_second) { return add(a_second); }

	// Subtraction
	Vector2 subtract(Vector2 & a_second);
	Vector2 operator - (Vector2 & a_second) { return subtract(a_second); }
	Vector2 operator -= (Vector2 a_second) { return subtract(a_second); }

	// Multiplication
	Vector2 multiply(float a_scalar, const Vector2 & a_second);
	Vector2 multiply(float a_scalar);
	Vector2 operator * (float a_scalar) { return multiply(a_scalar); }
	friend Vector2 operator * (float a_scalar, const Vector2 & a_second) { Vector2 v; return v.multiply(a_scalar, a_second); }

	// Division
	Vector2 divide(float a_scalar);
	Vector2 operator / (float a_scalar) { return divide(a_scalar); }

	// Other Operations //
	Vector2 operator = (Vector2 a_second);
	
	void scale(float a_scalar);
	void normalise();

	float dot(Vector2 a_second);
	float magnitude();

	explicit operator float* () { return &m_pos[0]; }
	
	union
	{
		struct { float m_pos[2]; };
		struct { float m_posX, m_posY; };
	};
};