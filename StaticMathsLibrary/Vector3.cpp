#include "Vector3.h"
#include "stdafx.h"
#include <math.h>

Vector3::Vector3()
{
}

Vector3::Vector3(float x, float y, float z)
{
	m_posX = x;
	m_posY = y;
	m_posZ = z;
}

Vector3::~Vector3()
{
}

Vector3 Vector3::operator + (const Vector3 & a_second)
{
	return Vector3(m_posX + a_second.m_posX, m_posY + a_second.m_posY, m_posZ + a_second.m_posZ);
}

Vector3 Vector3::operator = (Vector3 & a_second)
{
	return Vector3(m_posX = a_second.m_posX, m_posY = a_second.m_posY, m_posZ = a_second.m_posZ);
}

Vector3 Vector3::subtract(Vector3& a_second)
{
	return Vector3(m_posX - a_second.m_posX, m_posY - a_second.m_posY, m_posZ - a_second.m_posZ);
}

void Vector3::scale(float a_scalar)
{
	for (int i = 0; i != 2; i++)
	{
		m_pos[i] = m_pos[i] / a_scalar;
	}
}

float Vector3::dot (Vector3 a_second)
{
	return float((m_posX * a_second.m_posX) + (m_posY * a_second.m_posY) + (m_posZ * a_second.m_posZ));
}

Vector3 Vector3::multiply(float a_scalar)
{
	Vector3 v;

	v.m_posX = m_posX * a_scalar;
	v.m_posY = m_posY * a_scalar;
	v.m_posZ = m_posZ * a_scalar;

	return v;
}

Vector3 Vector3::multiply(float a_scalar, const Vector3 & a_second)
{
	return Vector3(a_scalar * a_second.m_posX, a_scalar * a_second.m_posY, a_scalar * a_second.m_posZ);
}

Vector3 Vector3::divide(float a_scalar)
{
	Vector3 sum;
	for (int i = 0; i != 2; i++)
	{
		sum.m_pos[i] = m_pos[i] / a_scalar;
	}
	return sum;
}

Vector3 Vector3::cross(Vector3 a_second)
{
	Vector3 v;

	v.m_posX = ((m_posY * a_second.m_posZ) - (m_posZ * a_second.m_posY));
	v.m_posY = ((m_posZ * a_second.m_posX) - (m_posX * a_second.m_posZ));
	v.m_posZ = ((m_posX * a_second.m_posY) - (m_posY * a_second.m_posX));

	return v;
}

void Vector3::normalise()
{
	float mag = magnitude();

	m_posX = m_posX / mag;
	m_posY = m_posY / mag;
	m_posZ = m_posZ / mag;
}

float Vector3::magnitude () { return (sqrt((m_posX * m_posX) + (m_posY * m_posY) + (m_posZ * m_posZ))); }