#pragma once

class Vector3
{
public:
	Vector3();
	Vector3(float x, float y, float z);

	~Vector3();

	//Addition
	Vector3 operator + (const Vector3 & a_second);
	Vector3 operator = (Vector3 & a_second);

	//Subtraction
	Vector3 subtract(Vector3 & a_second);
	Vector3 operator - (Vector3 & a_second) { return subtract(a_second); }

	//Multiply
	Vector3 multiply(float a_scalar);
	Vector3 operator * (float a_scalar) { return multiply(a_scalar); }
	
	Vector3 multiply(float a_scalar, const Vector3 & a_second);
	friend Vector3 operator * (float a_scalar, const Vector3 & a_second) { Vector3 v; return v.multiply(a_scalar, a_second); }
	
	Vector3 divide(float a_scalar);
	Vector3 operator / (float a_scalar) { return divide(a_scalar); }

	float magnitude();

	void scale(float a_scalar);

	float dot(Vector3 a_second);

	Vector3 cross(Vector3 a_second);
	void normalise();

	explicit operator float* () { return &m_pos[0]; }

	union
	{
		struct { float m_posX, m_posY, m_posZ; };
		struct { float m_pos[3]; };
	};
};

