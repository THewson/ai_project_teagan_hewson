#include "Matrix2.h"
#include "stdafx.h"

Matrix2::Matrix2()
{
}


Matrix2::~Matrix2()
{
}

Matrix2::Matrix2(float a_00, float a_01, float a_10, float a_11)
{
	m_matrix[0][0] = a_00;
	m_matrix[0][1] = a_01;
	m_matrix[1][0] = a_10;
	m_matrix[1][1] = a_11;
}

void Matrix2::setRotate(float a_rotator)
{
	 reassignMatrixValues(cosf(a_rotator), sinf(a_rotator), -sinf(a_rotator), cosf(a_rotator));
}

void Matrix2::setIdentityMatrix()
{
	m_m0 = 1;
	m_m1 = 0;
	m_m2 = 0;
	m_m3 = 1;
}

Matrix2 Matrix2::multiply(Matrix2 a_second)
{
	Matrix2 sum;

	sum.m_matrix[0][0] = (m_matrix[0][0] * a_second.m_matrix[0][0]) + (m_matrix[0][1] * a_second.m_matrix[1][0]);
	sum.m_matrix[0][1] = (m_matrix[0][0] * a_second.m_matrix[0][1]) + (m_matrix[0][1] * a_second.m_matrix[1][1]);
	sum.m_matrix[1][0] = (m_matrix[1][0] * a_second.m_matrix[0][0]) + (m_matrix[1][1] * a_second.m_matrix[1][0]);
	sum.m_matrix[1][1] = (m_matrix[1][0] * a_second.m_matrix[0][1]) + (m_matrix[1][1] * a_second.m_matrix[1][1]);
	
	return sum;
}

Vector2 Matrix2::multiply(Vector2 & a_second)
{
	return Vector2(
		(m_m0 * a_second.m_posX) + (m_m2 * a_second.m_posY), 
		(m_m1 * a_second.m_posX) + (m_m3 * a_second.m_posY));
}

void Matrix2::reassignMatrixValues(float m00, float m01, float m10, float m11)
{
	m_m0 = m00;
	m_m1 = m01;
	m_m2 = m10;
	m_m3 = m11;
}

