#include "Matrix4.h"
#include "stdafx.h"


Matrix4::Matrix4()
{
	// 1 0 0 0
	// 0 1 0 0 
	// 0 0 1 0
	// 0 0 0 1

	// First row;	
	m_m[0] = 1.f;
	m_m[1] = 0.f;
	m_m[2] = 0.f;
	m_m[3] = 0.f;

	// Second row;
	m_m[4] = 0.f;
	m_m[5] = 1.f;
	m_m[6] = 0.f;
	m_m[7] = 0.f;

	// Third row;
	m_m[8] =  0.f;
	m_m[9] = 0.f;
	m_m[10] = 1.f;
	m_m[11] = 0.f;

	// Fourth row;
	m_m[12] = 0.f;
	m_m[13] = 0.f;
	m_m[14] = 0.f;
	m_m[15] = 1.f;
}


Matrix4::~Matrix4()
{
}

Matrix4::Matrix4(float a_00, float a_01, float a_02, float a_03,
	float a_10, float a_11, float a_12, float a_13,
	float a_20, float a_21, float a_22, float a_23,
	float a_30, float a_31, float a_32, float a_33)
{
	m_matrix[0][0] = a_00;
	m_matrix[0][1] = a_01;
	m_matrix[0][2] = a_02;
	m_matrix[0][3] = a_03;

	m_matrix[1][0] = a_10;
	m_matrix[1][1] = a_11;
	m_matrix[1][2] = a_12;
	m_matrix[1][3] = a_13;

	m_matrix[2][0] = a_20;
	m_matrix[2][1] = a_21;
	m_matrix[2][2] = a_22;
	m_matrix[2][3] = a_23;

	m_matrix[3][0] = a_30;
	m_matrix[3][1] = a_31;
	m_matrix[3][2] = a_32;
	m_matrix[3][3] = a_33;
}

void Matrix4::setRotateX(float a_rotator)
{
	m_matrix[1][1] = cosf(a_rotator);
	m_matrix[1][2] = sinf(a_rotator);
	m_matrix[2][1] = -sinf(a_rotator);
	m_matrix[2][2] = cosf(a_rotator);
}

void Matrix4::setRotateY(float a_rotator)
{
	m_matrix[0][0] = cosf(a_rotator);
	m_matrix[0][2] = -sinf(a_rotator);
	m_matrix[2][0] = sinf(a_rotator);
	m_matrix[2][2] = cosf(a_rotator);
}

void Matrix4::setRotateZ(float a_rotator)
{
	m_matrix[0][0] = cosf(a_rotator);
	m_matrix[1][0] = -sinf(a_rotator);
	m_matrix[0][1] = sinf(a_rotator);
	m_matrix[1][1] = cosf(a_rotator);
}

Vector4 Matrix4::multiply(Vector4 a_second)
{
	return Vector4(
		m_vector[0].m_posX * a_second.m_posX + m_vector[1].m_posX * a_second.m_posY + m_vector[2].m_posX*a_second.m_posZ + m_vector[3].m_posX*a_second.m_posW,
		m_vector[0].m_posY * a_second.m_posX + m_vector[1].m_posY * a_second.m_posY + m_vector[2].m_posY*a_second.m_posZ + m_vector[3].m_posY*a_second.m_posW,
		m_vector[0].m_posZ * a_second.m_posX + m_vector[1].m_posZ * a_second.m_posY + m_vector[2].m_posZ*a_second.m_posZ + m_vector[3].m_posZ*a_second.m_posW,
		m_vector[0].m_posW * a_second.m_posX + m_vector[1].m_posW * a_second.m_posY + m_vector[2].m_posW*a_second.m_posZ + m_vector[3].m_posW*a_second.m_posW
	);
}

Matrix4 Matrix4::multiply(Matrix4 a_second)
{
	return Matrix4(
		(m_m[0] * a_second.m_m[0]) + (m_m[4] * a_second.m_m[1]) + (m_m[8] * a_second.m_m[2]) + (m_m[12] * a_second.m_m[3]),
		(m_m[1] * a_second.m_m[0]) + (m_m[5] * a_second.m_m[1]) + (m_m[9] * a_second.m_m[2]) + (m_m[13] * a_second.m_m[3]),
		(m_m[2] * a_second.m_m[0]) + (m_m[6] * a_second.m_m[1]) + (m_m[10] * a_second.m_m[2]) + (m_m[14]* a_second.m_m[3]),
		(m_m[3] * a_second.m_m[0]) + (m_m[7] * a_second.m_m[1]) + (m_m[11] * a_second.m_m[2]) + (m_m[15]* a_second.m_m[3]),
		  							   
		(m_m[0] * a_second.m_m[4]) + (m_m[4] * a_second.m_m[5]) + (m_m[8] * a_second.m_m[6]) + (m_m[12] * a_second.m_m[7]),
		(m_m[1] * a_second.m_m[4]) + (m_m[5] * a_second.m_m[5]) + (m_m[9] * a_second.m_m[6]) + (m_m[13] * a_second.m_m[7]),
		(m_m[2] * a_second.m_m[4]) + (m_m[6] * a_second.m_m[5]) + (m_m[10] * a_second.m_m[6]) + (m_m[14] * a_second.m_m[7]),
		(m_m[3] * a_second.m_m[4]) + (m_m[7] * a_second.m_m[5]) + (m_m[11] * a_second.m_m[6]) + (m_m[15] * a_second.m_m[7]),
		  							   
		(m_m[0] * a_second.m_m[8]) + (m_m[4] * a_second.m_m[9]) + (m_m[8] * a_second.m_m[10]) + (m_m[12] * a_second.m_m[11]),
		(m_m[1] * a_second.m_m[8]) + (m_m[5] * a_second.m_m[9]) + (m_m[9] * a_second.m_m[10]) + (m_m[13] * a_second.m_m[11]),
		(m_m[2] * a_second.m_m[8]) + (m_m[6] * a_second.m_m[9]) + (m_m[10] * a_second.m_m[10]) + (m_m[14] * a_second.m_m[11]),
		(m_m[3] * a_second.m_m[8]) + (m_m[7] * a_second.m_m[9]) + (m_m[11] * a_second.m_m[10]) + (m_m[15] * a_second.m_m[11]),
		  
		(m_m[0] * a_second.m_m[12]) + (m_m[4] * a_second.m_m[13]) + (m_m[8] * a_second.m_m[14]) + (m_m[12] * a_second.m_m[15]),
		(m_m[1] * a_second.m_m[12]) + (m_m[5] * a_second.m_m[13]) + (m_m[9] * a_second.m_m[14]) + (m_m[13] * a_second.m_m[15]),
		(m_m[2] * a_second.m_m[12]) + (m_m[6] * a_second.m_m[13]) + (m_m[10] * a_second.m_m[14]) + (m_m[14] * a_second.m_m[15]),
		(m_m[3] * a_second.m_m[12]) + (m_m[7] * a_second.m_m[13]) + (m_m[11] * a_second.m_m[14]) + (m_m[15] * a_second.m_m[15]));
}
