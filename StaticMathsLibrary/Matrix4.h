#pragma once
#include "Vector4.h"
#include <cmath>

class Matrix4
{
public:
	Matrix4();
	~Matrix4();
	Matrix4 (float a_00, float a_01, float a_02, float a_03,
			 float a_10, float a_11, float a_12, float a_13,
		     float a_20, float a_21, float a_22, float a_23,
		 	 float a_30, float a_31, float a_32, float a_33);

	void setRotateX(float a_rotator);
	void setRotateY(float a_rotator);
	void setRotateZ(float a_rotator);

	Vector4 multiply(Vector4 a_second);
	Matrix4 multiply(Matrix4 a_second);

	Vector4 operator * (Vector4 a_second) { return multiply(a_second); }
	Matrix4 operator * (Matrix4 a_second) { return multiply(a_second); }

	Vector4 & operator [] (int a_indexNum) { return m_vector[a_indexNum]; }

	explicit operator float* () { return &m_matrix[0][0]; }

	union
	{
		struct { float m_matrix[4][4]; };
		struct { float m_m[16]; };
		struct { Vector4 m_vector[4]; };
	};
};

