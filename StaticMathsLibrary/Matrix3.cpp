#include "Matrix3.h"
#include "stdafx.h"
#include <cmath>

Matrix3::Matrix3()
{
	m_m[0] = 1; 
	m_m[1] = 0;
	m_m[2] = 0;

	m_m[3] = 0;
	m_m[4] = 1;
	m_m[5] = 0;

	m_m[6] = 0;
	m_m[7] = 0;
	m_m[8] = 1;
}

Matrix3::~Matrix3()
{
}

Matrix3::Matrix3(float a_00, float a_01, float a_02, float a_10, float a_11, float a_12, float a_20, float a_21, float a_22)
{
	m_m[0] = a_00;
	m_m[1] = a_01;
	m_m[2] = a_02;

	m_m[3] = a_10;
	m_m[4] = a_11;
	m_m[5] = a_12;

	m_m[6] = a_20;
	m_m[7] = a_21;
	m_m[8] = a_22;
}

// Multiplies the Matrixs
Matrix3 Matrix3::multiply(Matrix3 & a_second)
{
	
	/*return Matrix3(
	m_m[0] = ((m_m[0] * a_second.m_m[0]) + (m_m[1] * a_second.m_m[3]) + (m_m[2] * a_second.m_m[6])),
	m_m[1] = ((m_m[0] * a_second.m_m[1]) + (m_m[1] * a_second.m_m[4]) + (m_m[2] * a_second.m_m[7])),
	m_m[2] = ((m_m[0] * a_second.m_m[2]) + (m_m[1] * a_second.m_m[5]) + (m_m[2] * a_second.m_m[8])),
																						  
	m_m[3] = ((m_m[3] * a_second.m_m[0]) + (m_m[4] * a_second.m_m[3]) + (m_m[5] * a_second.m_m[6])),
	m_m[4] = ((m_m[3] * a_second.m_m[1]) + (m_m[4] * a_second.m_m[4]) + (m_m[5] * a_second.m_m[7])),
	m_m[5] = ((m_m[3] * a_second.m_m[2]) + (m_m[4] * a_second.m_m[5]) + (m_m[5] * a_second.m_m[8])),
	 																					  
	m_m[6] = ((m_m[6] * a_second.m_m[0]) + (m_m[7] * a_second.m_m[3]) + (m_m[8] * a_second.m_m[6])),
	m_m[7] = ((m_m[6] * a_second.m_m[1]) + (m_m[7] * a_second.m_m[4]) + (m_m[8] * a_second.m_m[7])),
	m_m[8] = ((m_m[6] * a_second.m_m[2]) + (m_m[7] * a_second.m_m[5]) + (m_m[8] * a_second.m_m[8]))
	);*/

	return Matrix3
	(
		(m_m[0] * a_second.m_m[0]) + (m_m[3] * a_second.m_m[1]) + (m_m[6] * a_second.m_m[2]), (m_m[1] * a_second.m_m[0]) + (m_m[4] * a_second.m_m[1]) + (m_m[7] * a_second.m_m[2]), (m_m[2] * a_second.m_m[0]) + (m_m[5] * a_second.m_m[1]) + (m_m[8] * a_second.m_m[2]),
		(m_m[0] * a_second.m_m[3]) + (m_m[3] * a_second.m_m[4]) + (m_m[6] * a_second.m_m[5]), (m_m[1] * a_second.m_m[4]) + (m_m[4] * a_second.m_m[4]) + (m_m[7] * a_second.m_m[5]), (m_m[2] * a_second.m_m[3]) + (m_m[5] * a_second.m_m[4]) + (m_m[8] * a_second.m_m[5]),
		(m_m[0] * a_second.m_m[6]) + (m_m[3] * a_second.m_m[7]) + (m_m[6] * a_second.m_m[8]), (m_m[1] * a_second.m_m[6]) + (m_m[4] * a_second.m_m[7]) + (m_m[7] * a_second.m_m[8]), (m_m[2] * a_second.m_m[6]) + (m_m[5] * a_second.m_m[7]) + (m_m[8] * a_second.m_m[8]));

}

Vector3 Matrix3::multiply(Vector3 a_second)
{
	return Vector3(
		(m_vector[0].m_posX * a_second.m_posX) + (m_vector[1].m_posX * a_second.m_posY) + (m_vector[2].m_posX * a_second.m_posZ),
		(m_vector[0].m_posY * a_second.m_posX) + (m_vector[1].m_posY * a_second.m_posY) + (m_vector[2].m_posY * a_second.m_posZ),
		(m_vector[0].m_posZ * a_second.m_posX) + (m_vector[1].m_posZ * a_second.m_posY) + (m_vector[2].m_posZ * a_second.m_posZ));
}

Matrix3 Matrix3::operator = (Matrix3 a_second)
{
	return Matrix3(
	m_m[0] = a_second.m_m[0],
	m_m[1] = a_second.m_m[1],
	m_m[2] = a_second.m_m[2],
		  
	m_m[3] = a_second.m_m[3],
	m_m[4] = a_second.m_m[4],
	m_m[5] = a_second.m_m[5],
	 		 				
	m_m[6] = a_second.m_m[6],
	m_m[7] = a_second.m_m[7],
	m_m[8] = a_second.m_m[8]);
}

void Matrix3::reassignMatrixValues(float a_00, float a_01, float a_02, float a_10, float a_11, float a_12, float a_20, float a_21, float a_22)
{
	m_m[0] = a_00;
	m_m[1] = a_01;
	m_m[2] = a_02;
		
	m_m[3] = a_10;
	m_m[4] = a_11;
	m_m[5] = a_12;
		
	m_m[6] = a_20;
	m_m[7] = a_21;
	m_m[8] = a_22;
}

void Matrix3::setRotateX(float a_rotator)
{
	m_m[4] = cos(a_rotator);
	m_m[5] = sin(a_rotator);
	m_m[7] = -sin(a_rotator);
	m_m[8] = cos(a_rotator);
}

void Matrix3::setRotateY(float a_rotator)
{
	m_m[0] = cos(a_rotator);
	m_m[6] = sin(a_rotator);
	m_m[2] = -sin(a_rotator);
	m_m[8] = cos(a_rotator);
}

void Matrix3::setRotateZ(float a_rotator)
{
	m_m[0] = cos(a_rotator);
	m_m[1] = sin(a_rotator);
	m_m[3] = -sin(a_rotator);
	m_m[4] = cos(a_rotator);
}