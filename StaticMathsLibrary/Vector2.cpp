#include "Vector2.h"
#include "stdafx.h"

Vector2::Vector2() 
{
}

Vector2::Vector2(float x, float y)
{
	m_posX = x;
	m_posY = y;
}

Vector2::~Vector2()
{
}

Vector2 Vector2::add(Vector2 & a_second)
{
	Vector2 sum;
	
	sum.m_posX = m_posX + a_second.m_posX;
	sum.m_posY = m_posY + a_second.m_posY;

	return sum;
}

Vector2 Vector2::subtract(Vector2 & a_second)
{
	Vector2 sum;

	sum.m_posX = m_posX - a_second.m_posX;
	sum.m_posY = m_posY - a_second.m_posY;
	
	return sum;
}

Vector2 Vector2::operator = (Vector2 a_second)
{
	return Vector2(m_posX = a_second.m_posX, m_posY = a_second.m_posY);
}

void Vector2::scale(float a_scalar)
{
	for (int i = 0; i != 1; i++)
	{
		m_pos[i] = m_pos[i] / a_scalar;
	}
}

Vector2 Vector2::multiply(float a_scalar, const Vector2 & a_second)
{
	return Vector2(a_scalar * a_second.m_posX, a_scalar * a_second.m_posY);
}

Vector2 Vector2::multiply(float a_scalar)
{
	Vector2 sum;

	sum.m_posX = m_posX * a_scalar;
	sum.m_posY = m_posY * a_scalar;

	return sum;
}

Vector2 Vector2::divide(float a_scalar)
{
	Vector2 sum;
	for (int i = 0; i != 1; i++)
	{
		sum.m_pos[i] = m_pos[i] / a_scalar;
	}

	return sum;
}

float Vector2::dot(Vector2 a_second)
{
	return float((m_posX * a_second.m_posX) + (m_posY * a_second.m_posY));
}

float Vector2::magnitude() { return (sqrt((m_posX * m_posX) + (m_posY * m_posY))); }

void Vector2::normalise()
{
	float mag = magnitude();

	m_posX = m_posX / mag;
	m_posY = m_posY / mag;
}
