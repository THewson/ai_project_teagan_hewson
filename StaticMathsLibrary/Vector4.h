#pragma once
#include <cmath>

class Vector4
{
public:
	Vector4();
	Vector4(float x, float y, float z, float w);
	~Vector4();

	// Addition
	Vector4 add(Vector4 & a_second);
	Vector4 operator + (Vector4 & a_second) { return add(a_second); }

	// Subtraction
	Vector4 subtract(Vector4 & a_second);
	Vector4 operator - (Vector4 & a_second) { return subtract(a_second); }

	// Multiply
	Vector4 multiply(float a_scalar);
	Vector4 multiply(float a_scalar, Vector4 a_second);
	Vector4 operator * (float a_scalar) { return multiply(a_scalar); }
	friend Vector4 operator * (float a_scalar, Vector4 a_second) { Vector4 v; return v.multiply(a_scalar, a_second); }
	
	// Divide
	Vector4 divide(float a_scalar);
	Vector4 operator / (float a_scalar) { return divide(a_scalar); }

	float magnitude();

	void scale(float a_scalar);
	Vector4 cross(Vector4 a_second);
	float dot(Vector4 a_second);

	void normalise();

	explicit operator float* () { return &m_pos[0]; }

	union
	{
		struct { float m_pos[4]; };
		struct { float m_posX, m_posY, m_posZ, m_posW; };
	};
};

