#pragma once
#include "Vector3.h"
class Matrix3
{
public:
	Matrix3();
	~Matrix3();
	Matrix3(float a_00, float a_01, float a_02,
		float a_10, float a_11, float a_12,
		float a_20, float a_21, float a_22);

	void setRotateX(float a_rotator);
	void setRotateY(float a_rotator);
	void setRotateZ(float a_rotator);

	Matrix3 multiply(Matrix3 & a_second);
	Matrix3 operator * (Matrix3 & a_second) { return multiply(a_second); }

	Vector3 multiply(Vector3 a_second);
	Vector3 operator * (Vector3 a_second) { return multiply(a_second); }

	Vector3 & operator [] (int a_indexNum) { return m_vector[a_indexNum]; }

	Matrix3 operator = (Matrix3 a_second);
	explicit operator float* () { return &m_matrix[0][0]; }


	void reassignMatrixValues(float a_00, float a_01, float a_02,
		float a_10, float a_11, float a_12,
		float a_20, float a_21, float a_22);

	union
	{
		struct { float m_matrix[3][3]; };
		struct { float m_m[9]; };
		struct { Vector3 m_vector[3]; };
	};
};

