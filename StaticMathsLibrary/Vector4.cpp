#include "Vector4.h"
#include <math.h>
#include "stdafx.h"

Vector4::Vector4()
{
}

Vector4::Vector4(float x, float y, float z, float w) : m_posX(x), m_posY(y), m_posZ(z), m_posW(w)
{
}

Vector4::~Vector4()
{
}

Vector4 Vector4::add(Vector4 & a_second)
{
	return Vector4(m_posX + a_second.m_posX, m_posY + a_second.m_posY, m_posZ + a_second.m_posZ, m_posW + a_second.m_posW);
}

Vector4 Vector4::subtract (Vector4 & a_second)
{
	Vector4 sum;
	
	sum.m_posX = m_posX - a_second.m_posX;
	sum.m_posY = m_posY - a_second.m_posY;
	sum.m_posZ = m_posZ - a_second.m_posZ;
	sum.m_posW = m_posW - a_second.m_posW;

	return sum;
}

void Vector4::scale(float a_scalar)
{
	for (int i = 0; i != 3; i++)
	{
		m_pos[i] = m_pos[i] / a_scalar;
	}
}

Vector4 Vector4::multiply(float a_scalar)
{
	Vector4 sum (0,0,0,0);
	for (int i = 0; i != 3; i++)
	{
		sum.m_pos[i] = m_pos[i] * a_scalar;
	}

	return sum;
}

Vector4 Vector4::multiply(float a_scalar, Vector4 a_second)
{
	Vector4 sum;

	sum.m_posX = a_second.m_posX * a_scalar;
	sum.m_posY = a_second.m_posY * a_scalar;
	sum.m_posZ = a_second.m_posZ * a_scalar;
	sum.m_posW = a_second.m_posW * a_scalar;

	return sum;
}

Vector4 Vector4::divide(float a_scalar)
{
	Vector4 sum;
	for (int i = 0; i != 3; i++)
	{
		sum.m_pos[i] = m_pos[i] / a_scalar;
	}
	return sum;
}

float Vector4::magnitude()
{
	// Pythagoras Theorum
	return float(sqrt((m_pos[0] * m_pos[0]) + (m_pos[1] * m_pos[1]) + (m_pos[2] * m_pos[2])));
}

Vector4 Vector4::cross(Vector4 a_second)
{
	Vector4 sum (0,0,0,0);

	sum.m_posX = ((m_posY * a_second.m_posZ) - (m_posZ * a_second.m_posY));
	sum.m_posY = ((m_posZ * a_second.m_posX) - (m_posX * a_second.m_posZ));
	sum.m_posZ = ((m_posX * a_second.m_posY) - (m_posY * a_second.m_posX));
	sum.m_posW = ((m_posW * a_second.m_posW) - (m_posW * a_second.m_posW));

	return sum;
}

float Vector4::dot(Vector4 a_second)
{
	return float((m_posX * a_second.m_posX) + (m_posY * a_second.m_posY) + (m_posZ * a_second.m_posZ));
}

void Vector4::normalise() //Normalises the vector.
{
	float mag = magnitude();

	m_posX = m_posX / mag;
	m_posY = m_posY / mag;
	m_posZ = m_posZ / mag;
	m_posW = m_posW / mag;
}
