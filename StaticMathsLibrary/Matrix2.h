#pragma once
#include "Vector2.h"
#include <math.h>
#define PI 3.14159265359

class Matrix2
{
public:
	Matrix2();
	~Matrix2();
	Matrix2(float a_00, float a_01, float a_10, float a_11);
	
	void setRotate(float a_rotator);
	void setIdentityMatrix();

	// Multiplication
	Matrix2 multiply(Matrix2 a_second);
	Matrix2 operator * (Matrix2 a_second) { return multiply(a_second); }
	
	Vector2 multiply(Vector2 & a_second);
	Vector2 operator * (Vector2 a_second) { return multiply(a_second); }

	explicit operator float* () { return &m_matrix[0][0]; }

	void reassignMatrixValues(float m00, float m01, float m10, float m11);

	union 
	{
		struct { float m_matrix[2][2]; };
		struct { float m_m[4]; };
		struct { float m_m0, m_m1, m_m2, m_m3; };
	};


};

