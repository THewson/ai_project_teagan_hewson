#pragma once

// Graph Definitions
#define GRAPH_SPACEBETWEEN 30
#define GRAPH_ROWS 13
#define GRAPH_COLS 24
#define GRAPH_XOFFSET 25
#define GRAPH_YOFFSET 25
#define GRAPH_EDGEMAXLENGTH 500