#include "BlackBoard.h"

BlackBoard::BlackBoard()
{
}

BlackBoard::~BlackBoard()
{
}

void BlackBoard::Update()
{
}

glm::vec2 * BlackBoard::GetVec2(int id)
{
	return &m_list[id]->m_position;
}

int BlackBoard::GetListLength()
{
	return m_listNum;
}

Agent* BlackBoard::getEntry(int id)
{
	return m_list[id];
}

void BlackBoard::setEntry(int id, Agent* item)
{
	m_list.push_back(item);
	m_listNum += 1;
}

bool BlackBoard::hasEntry(int id)
{
	if (id <= m_listNum)
	{
		return true;
	}
	else
	{
		return false;
	}

}
