#include "Agent.h"
#include <Input.h>
#include <Application.h>
#include <time.h>
#include "IBehaviour.h"
#include "BlackBoard.h"

Agent::Agent()
{
	srand(time(NULL));
	
	m_position.x = 300;
	m_position.y = 300;
	
	m_acceleration.x = 0;
	m_acceleration.y = 0;
	
	m_force.x = 0;
	m_force.y = 0;

	m_maxVelocity = 5;

	m_IBM = new IBehaviourManager();
	m_IBM->StartUp();
}
Agent::~Agent()
{
}

void Agent::Update(float deltaTime)
{
	m_IBM->Update(deltaTime);

	ApplyForce(2.f * -m_velocity);

	m_velocity += m_acceleration * deltaTime;
	m_position += m_velocity * deltaTime;
	m_acceleration = glm::vec2(0, 0);
}

float Agent::Magnitude(glm::vec2 vec) {
	float x = vec.x;
	float y = vec.y;
	return sqrt((x*x) + (y*y));
}

glm::vec2 Agent::Normalise(glm::vec2 vec) {
	glm::vec2 temp;
	float mag = Magnitude(vec);
	temp.x = vec.x / mag;
	temp.y = vec.y / mag;
	return temp;
}

void Agent::CalculateMovement(glm::vec2 * pos) { 
	glm::vec2 npos;
	npos.x = pos->x;
	npos.y = pos->y;

	m_velocity = Normalise(npos - m_position) * m_maxVelocity; 
}

void Agent::Teleport(float newX, float newY) {
	m_position.x = newX;
	m_position.y = newY;
}

void Agent::AssignBB(BlackBoard * bb)
{
	m_IBM->bboard = bb;
	m_IBM->me = this;
}

void Agent::GiveTarget(glm::vec2 tar)
{
	m_IBM->AssignTarget(tar);
}

void Agent::GiveBehaviour(int behav)
{
	m_IBM->m_currentBehaviour = m_IBM->m_behaviours[behav];
}

void Agent::ApplyForce(const glm::vec2 & force)
{
	m_acceleration += force;
}
