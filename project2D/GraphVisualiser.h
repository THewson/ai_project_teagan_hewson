#pragma once
#include <Renderer2D.h>
#include "Graph.h"
#include <glm/vec2.hpp>

class GraphVisualiser
{
public:
	GraphVisualiser();
	~GraphVisualiser();

	void Draw(aie::Renderer2D* renderer);

	void SetUpGraph();

	Graph<glm::vec2, float>::Node* ReturnRandomNode();

	Graph<glm::vec2, float> * m_Graph;
};

