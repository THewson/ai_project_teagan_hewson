#include "GraphRunner.h"

GraphRunner::GraphRunner()
{
}

GraphRunner::~GraphRunner()
{
}

Path GraphRunner::DijkstraShortestPath(Graph<glm::vec2, float>* graph, Graph<glm::vec2, float>::Node * destination, Graph<glm::vec2, float>::Node * start)
{
	for (auto it = graph->m_Node.begin(); it != graph->m_Node.end(); it++) {
		(*it)->visited = false;
	}

	openList.clear();
	std::list<Graph<glm::vec2, float>::Node *> closeList;
	closeList.clear();
	Graph<glm::vec2, float>::Node * currentNode;

	for (auto iter = graph->m_Node.begin(); iter != graph->m_Node.end(); iter++)
	{
		(*iter)->gScore = 99999999.0f;
		(*iter)->parent = nullptr;
	}
	start->parent = start;
	start->gScore = 0.0f;
	openList.push_back(start);

	auto compare = [](Graph<glm::vec2, float>::Node* left, Graph<glm::vec2, float>::Node* right) {
		return left->gScore < right->gScore;
	};

	while (!openList.empty())
	{
		openList.sort(compare);
		currentNode = openList.front();
		closeList.push_back(currentNode);

		currentNode->visited = true;
		openList.pop_front();

		if (currentNode == destination)
		{
			break;
		}
		else
		{
			for (auto iter = currentNode->edges.begin(); iter != currentNode->edges.end(); iter++)
			{
				if (std::find(closeList.begin(), closeList.end(), (*iter)->target) == closeList.end()) {
				
					if ((*iter)->target->gScore > currentNode->gScore + (*iter)->cost){
					
						(*iter)->target->parent = currentNode;
						(*iter)->target->gScore = (currentNode->gScore + (*iter)->cost);

						if (std::find(openList.begin(), openList.end(), (*iter)->target) == openList.end()) {
							openList.push_back((*iter)->target);
						}
					}
				}
			}
		}
	}

	currentNode = destination;

	while (currentNode != currentNode->parent)
	{
		m_path.PushPathSeg(currentNode);
		currentNode = currentNode->parent;
	}

	std::reverse(m_path.m_path.begin(), m_path.m_path.end());
	return m_path;
}

Path GraphRunner::AStarPath(Graph<glm::vec2, float>* graph, Graph<glm::vec2, float>::Node * destination, Graph<glm::vec2, float>::Node * start)
{

	for (auto it = graph->m_Node.begin(); it != graph->m_Node.end(); it++) {
		(*it)->visited = false;
	}

	for (auto iter = graph->m_Node.begin(); iter != graph->m_Node.end(); iter++)
	{
		(*iter)->gScore = 9999999999999.0f;
		(*iter)->hScore = 0.0f;
		(*iter)->fScore = 0.0f;
		(*iter)->parent = nullptr;
	}

	m_path.ClearPath();
	openList.clear();
	closeList.clear();
	Graph<glm::vec2, float>::Node * currentNode;

	
	start->parent = start;
	start->gScore = 0.0f;
	openList.push_back(start);

	auto compare = [](Graph<glm::vec2, float>::Node* left, Graph<glm::vec2, float>::Node* right) {
		return left->fScore < right->fScore;
	};

	while (!openList.empty())
	{
		openList.sort(compare);
		currentNode = openList.front();
		closeList.push_back(currentNode);

		currentNode->visited = true;
		openList.pop_front();

		if (currentNode == destination)
		{
			break;
		}
		else
		{
			for (auto iter = currentNode->edges.begin(); iter != currentNode->edges.end(); iter++)
			{
				if ((*iter)->target->parent != destination) {

					if (std::find(closeList.begin(), closeList.end(), (*iter)->target) == closeList.end()) {
						float x = destination->m_NodeData - (*iter)->target->m_NodeData;
						float fScoreCurrent = currentNode->gScore + (*iter)->cost + glm::length(x);
						if ((*iter)->target->fScore < fScoreCurrent) {

							(*iter)->target->parent = currentNode;
							(*iter)->target->gScore = (currentNode->gScore + (*iter)->cost);
							(*iter)->target->hScore = glm::length(x);
							(*iter)->target->fScore = (*iter)->target->gScore + (*iter)->target->hScore;

							if (std::find(openList.begin(), openList.end(), (*iter)->target) == openList.end()) {
								openList.push_back((*iter)->target);
							}
						}
					}
				}
			}
		}
	}

	currentNode = destination;

	while (currentNode != currentNode->parent)
	{
		m_path.PushPathSeg(currentNode);
		currentNode = currentNode->parent;
	}

	std::reverse(m_path.m_path.begin(), m_path.m_path.end());
	return m_path;
}

void GraphRunner::Draw(aie::Renderer2D * renderer)
{
	renderer->setRenderColour(1.0f, 0.0f, 1.0f, 1.0f);
	n = 1;
	for (auto iter = m_path.m_path.begin(); iter != m_path.m_path.end(); iter++)
	{
		n += 0.1f;
		renderer->drawCircle((*iter)->m_NodeData->x, (*iter)->m_NodeData->y, n, 0.0f);
	}
}

//////////
// Path //
//////////

Path::Path()
{
}

Path::~Path()
{
}

void Path::PushPathSeg(Graph<glm::vec2, float>::Node * n)
{
	m_path.push_back(n);
}

void Path::PopPathSeg()
{
	if (m_path.empty() != true)
	{
		m_path.pop_back();
	}
}

void Path::ClearPath()
{
	m_path.clear();
}
