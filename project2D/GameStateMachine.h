#pragma once
#include <vector>
#include <assert.h>
#include "StateMachine.h"
#include "Renderer2D.h"


class GameState {
	friend class GameStateManager;
public:
	enum State {
		SPLASH, MENU, GAME, EXIT
	};
	GameState() {}
	virtual ~GameState() {}
	
	bool IsActive() const { return m_active; }
protected:
	virtual void OnInit() = 0;
	virtual void OnUpdate(float deltaTime) = 0;
	virtual void OnDraw(aie::Renderer2D* renderer) = 0;
	
	void enter()
	{
		m_active = true;
		OnEnter();
	}
	
	void exit()
	{
		m_active = false;
		OnExit();
	}

	// can be overwritten in derived classes that need to behave
	// in certain ways when activated/deactivated or pushed/popped

	virtual void OnEnter() {}
	virtual void OnExit() {}
	virtual void OnPushed() {}
	virtual void OnPopped() {}

private:
	bool m_active = false;
};

class GameStateManager {
public:
	GameStateManager(unsigned int stateCount) {
		m_registeredStates.resize(stateCount);
		assert(m_Manager == NULL);
		m_Manager = this;
	}

	GameStateManager() {
		m_registeredStates.resize(0);
		assert(m_Manager == NULL);
		m_Manager = this;
	}
	~GameStateManager()
	{
		for (auto state : m_registeredStates)
			delete state;
	}

	void RegisterState(int id, GameState* state);
	void PushState(int id);
	void PopState();
	void Update(float deltaTime);
	void Draw(aie::Renderer2D* renderer);
	int ActiveStateCount() const { return m_stateStack.size(); }
	GameState* GetTopState() const {
		if (m_stateStack.empty() == false)
		{
			return m_stateStack.back();
		}
		else
		{
			return nullptr;
		}
	}
	GameState* GetState(int id) const { return m_registeredStates[id]; }

	static GameStateManager* m_Manager;

	std::vector<GameState*> m_pushedStates;
	bool m_popState = false;

	std::vector<GameState*> m_stateStack;
	std::vector<GameState*> m_registeredStates;
};