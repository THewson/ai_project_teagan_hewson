#pragma once
#include "Application.h"
#include "Renderer2D.h"
#include "Audio.h"
#include "GraphVisualiser.h"
#include "GameObjectManager.h"
#include "Persuer.h"
#include "Hunted.h"
#include "GraphRunner.h"
#include "Mouse.h"

class BlackBoard;
class Application2D : public aie::Application {
public:

	Application2D();
	virtual ~Application2D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;

	float m_cameraX, m_cameraY;
	float m_timer;


	std::vector<Agent*> list;
	GameObjectManager * theMan;
	
	Graph<glm::vec2, float>::Node* s;
	Graph<glm::vec2, float>::Node* d;
	GraphRunner * initialD;
	BlackBoard * blackBoard;
	GraphVisualiser * graph;

	Hunted* hunt;
	Persuer* pers;
};