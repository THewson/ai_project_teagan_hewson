#include "Hunted.h"
#include "IBehaviour.h"


Hunted::Hunted()
{
	AddBehaviours();
	m_position.x = 200;
	m_position.y = 200;
}


Hunted::~Hunted()
{
}

void Hunted::AddBehaviours()
{
	m_IBM->m_behaviours.push_back(new Controller);
	m_IBM->m_behaviours.back()->pAgent = this;

	m_IBM->m_behaviours.push_back(new Flee);
	m_IBM->m_behaviours.back()->pAgent = this;

	m_IBM->m_currentBehaviour = m_IBM->m_behaviours[0];
	m_IBM->m_currentBehaviour->Transition();
}


