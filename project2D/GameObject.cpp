#include "GameObject.h"

GameObject::GameObject()
 { m_rotation.setIdentityMatrix(); }


void GameObject::Draw(aie::Renderer2D * renderer)
{
		renderer->drawSpriteTransformed3x3(m_texture, (float*)& m_globalTransform);
		Entity::Draw(renderer);
}
