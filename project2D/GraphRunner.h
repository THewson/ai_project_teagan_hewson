#pragma once
#include "GraphVisualiser.h"
#include <list>
#include <queue>


class Path
{
public:
	Path();
	~Path();

	void PushPathSeg(Graph<glm::vec2, float>::Node * n);
	void PopPathSeg();

	void ClearPath();

	std::vector<Graph<glm::vec2, float>::Node*> m_path;

};

class GraphRunner
{
public:
	GraphRunner();
	~GraphRunner();
	
	Path DijkstraShortestPath(Graph<glm::vec2, float> * graph,
		Graph<glm::vec2, float>::Node* destination,
		Graph<glm::vec2, float>::Node * start);

	Path AStarPath(Graph<glm::vec2, float> * graph,
		Graph<glm::vec2, float>::Node* destination,
		Graph<glm::vec2, float>::Node * start);

	void Draw(aie::Renderer2D * renderer);

	Graph<glm::vec2, float>::Node * m_currentpos;
	std::list<Graph<glm::vec2, float>::Node *> openList;
	std::list<Graph<glm::vec2, float>::Node *> closeList;
	Path m_path;
	float n;
};