#include "Mouse.h"
#include "IBehaviour.h"
#include <Input.h>

Mouse::Mouse()
{
	AddBehaviours();
}


Mouse::~Mouse()
{
}

void Mouse::AddBehaviours()
{
	m_IBM->m_currentBehaviour = new Controller;
	m_IBM->m_currentBehaviour->pAgent = this;
}
