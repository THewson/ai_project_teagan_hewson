#include "Persuer.h"
#include "IBehaviour.h"


Persuer::Persuer()
{
	AddBehaviours();
	m_position.x = 100;
	m_position.y = 100;
}


Persuer::~Persuer()
{
}

void Persuer::AddBehaviours()
{
	m_IBM->m_behaviours.push_back(new Seek);
	m_IBM->m_behaviours.back()->pAgent = this;

	m_IBM->m_behaviours.push_back(new Wander);
	m_IBM->m_behaviours.back()->pAgent = this;

	m_IBM->m_currentBehaviour = m_IBM->m_behaviours[1];
	m_IBM->m_currentBehaviour->Transition();
}
