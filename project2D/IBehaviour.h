#pragma once
#include "Agent.h"
#include <glm\vec2.hpp>
#include <glm\glm.hpp>

#include <time.h>
#include <vector>
#include "BlackBoard.h"
#include <functional>

class IBehaviour
{
public:
	IBehaviour();
	~IBehaviour();
	virtual void Update(float deltaTime) = 0;
	virtual void Transition() = 0;

	Agent* pAgent = nullptr;
	glm::vec2 m_targetPos;
};

class IBehaviourManager
{
public:
	void StartUp();
	void Update(float deltaTime);
	void AssignTarget(glm::vec2 tar); // REWORK
	std::vector< IBehaviour * > m_behaviours;
	IBehaviour* m_currentBehaviour;
	int m_currentBehaviourNum;
	BlackBoard * bboard;
	const Agent* me;

	void CheckCircle();

	void OnRadiusEnter(std::function<void()> func);
	void OnRadiusExit(std::function<void()> func);
	bool isInside;

	float lastDistanceToTarget;
	float distanceToTarget;
	glm::vec2 lastPosition;

	std::function<void()> m_OnRadiusEnter;
	std::function<void()> m_OnRadiusExit;

	float innerRadius;
};

class Seek : public IBehaviour
{
	virtual void Transition();
	virtual void Update(float deltaTime);
};

class Flee : public IBehaviour
{	
	virtual void Transition();
	virtual void Update(float deltaTime);
};

class Controller : public IBehaviour
{
	virtual void Transition();
	virtual void Update(float deltaTime);
};

class Wander : public IBehaviour
{
	virtual void Transition() {
		circleRadius = 100;
		wanderAngle = 0.f;
		circleCentre.x = 0.f;
		circleCentre.y = 0.f;
	}
	virtual void Update(float deltaTime);

	glm::vec2 circleCentre;
	glm::vec2 displacement;

	float wanderAngle;
	int circleRadius;

};
