#pragma once
#include "GameStateMachine.h"
#include "Input.h"
#include "Agent.h"
#include "Utilities.h"
#include "GraphVisualiser.h"


class PlayState : public GameState
{
public:
	PlayState();
	~PlayState() {}
	glm::vec2 * mouse;


	virtual void OnInit();
	virtual void OnUpdate(float deltaTime);
	virtual void OnDraw(aie::Renderer2D* renderer);


	//GraphVisualiser m_Graph;
	Agent * m_A1;
	//Agent * m_Pursuer;
	bool win = false;
	int sx, sy;
};