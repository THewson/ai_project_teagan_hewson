#include "Application2D.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "BlackBoard.h"

Application2D::Application2D() {

}

Application2D::~Application2D() {

}

bool Application2D::startup() {
	
	m_2dRenderer = new aie::Renderer2D();
	graph = new GraphVisualiser();
	graph->SetUpGraph();
	s = new Graph<glm::vec2, float>::Node;
	d = new Graph<glm::vec2, float>::Node;
	theMan = new GameObjectManager();
	blackBoard = new BlackBoard();

	hunt = new Hunted;
	pers = new Persuer;
	
	list.push_back(hunt);
	list.push_back(pers);
	theMan->AddList(list, blackBoard);

	initialD = new GraphRunner();

	m_font = new aie::Font("./font/consolas.ttf", 32);

	m_cameraX = 0;
	m_cameraY = 0;
	m_timer = 0;

	return true;
}


void Application2D::shutdown() {
	delete hunt;
	delete pers;
	delete graph;
	delete blackBoard;
	delete initialD;
	delete theMan;
	delete m_font;
	delete m_2dRenderer;
}

void Application2D::update(float deltaTime) {
	m_timer += deltaTime;
	// input example
	aie::Input* input = aie::Input::getInstance();

	hunt->GiveTarget(pers->m_position);
	pers->GiveTarget(hunt->m_position);

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE)) {quit();}

	if (input->wasKeyPressed(aie::INPUT_KEY_1))
	{
		s = graph->ReturnRandomNode();
		d = graph->ReturnRandomNode();
		while (s == d)
		{
			d = graph->ReturnRandomNode();
		}

		initialD->DijkstraShortestPath(graph->m_Graph, d, s);
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_2))
	{
		s = graph->ReturnRandomNode();
		d = graph->ReturnRandomNode();
		while (s == d)
		{
			d = graph->ReturnRandomNode();
		}

		initialD->AStarPath(graph->m_Graph, d, s);
	}

	theMan->Update(deltaTime);
}

void Application2D::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// set the camera position before we begin rendering
	m_2dRenderer->setCameraPos(m_cameraX, m_cameraY);

	// begin drawing sprites
	m_2dRenderer->begin();
	graph->Draw(m_2dRenderer);

	m_2dRenderer->setRenderColour(1.0f, 0.25f, 0.25f, 0.5f);
	if(s->m_NodeData != nullptr)
	m_2dRenderer->drawCircle(s->m_NodeData->x, s->m_NodeData->y, 14, 0.0f);

	if (d->m_NodeData != nullptr)
	m_2dRenderer->drawCircle(d->m_NodeData->x, d->m_NodeData->y, 14, 0.0f);
	initialD->Draw(m_2dRenderer);

	theMan->Draw(m_2dRenderer);

	m_2dRenderer->drawText(m_font, "Press 1 for: Dijkstra, 2 for A*.", 0, 720 - 64);

	// done drawing sprites
	m_2dRenderer->end();
}