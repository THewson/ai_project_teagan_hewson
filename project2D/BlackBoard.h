#pragma once
#include <list>

#include <glm\vec2.hpp>

#include <map>
#include "Agent.h"
#include <vector>
class BlackBoard
{
	enum eBlackboardDataType {
		type_float,
		type_int,
		type_char,
		type_bool,
		type_pointer
	};

	struct BlackBoardData {
		eBlackboardDataType type;
	};


	union {
		float floatValue;
		int intValue;
		char charValue;
		bool boolValue;
		void* pointerValue;
	};

public:
	BlackBoard();
	~BlackBoard();

	void Update();

	// Functions to place in Update
	glm::vec2* GetVec2(int id);
	int GetListLength();
	//returns true if ID exists
	Agent* getEntry(int id);
	void setEntry(int id, Agent* item);
	bool hasEntry(int id);

protected:
	std::vector<Agent*> m_list;
	int m_listNum;
};

