#include "GraphVisualiser.h"
#include "Utilities.h"


GraphVisualiser::GraphVisualiser()
{

}


GraphVisualiser::~GraphVisualiser()
{

}

void GraphVisualiser::Draw(aie::Renderer2D * renderer)
{
	auto &nodeList = m_Graph->GetNodes();
	

	for (auto nodeIter = nodeList.begin(); nodeIter != nodeList.end(); nodeIter++)
	{
		Graph<glm::vec2, float>::Node* nodeA = (*nodeIter);
		for (auto eIter = nodeA->edges.begin(); eIter != nodeA->edges.end(); eIter++)
		{
			Graph<glm::vec2, float>::Node* nodeB = (*eIter)->target;
			

			renderer->drawLine(nodeA->m_NodeData->x, nodeA->m_NodeData->y, nodeB->m_NodeData->x, nodeB->m_NodeData->y, 2.f);
		}
	}

	renderer->setRenderColour(1.0f, 0.5f, 0.5f, 1.0f);

	for (auto iter = nodeList.begin(); iter != nodeList.end(); iter++)
	{
		Graph<glm::vec2, float>::Node *node = (*iter);
		if (node->visited == true)
			{
				renderer->setRenderColour(1.0f, 0.5f, 0.5f, 1.0f);
			}
		else
			{
				renderer->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
			}
		renderer->drawCircle(node->m_NodeData->x, node->m_NodeData->y, 5);
	}
}

void GraphVisualiser::SetUpGraph()
{
	m_Graph = new Graph<glm::vec2, float>();

	for (int y = 0; y < GRAPH_ROWS; y++)
	{
		for (int x = 0; x < GRAPH_COLS; x++)
		{
			glm::vec2 * v = new glm::vec2(50 + x * 50, 50 + y * 50);
			m_Graph->AddNode(v);
		}
	}


	for (int y = 0; y < GRAPH_ROWS; y++)
	{
		for (int x = 0; x < GRAPH_COLS; x++)
		{
			int n = y * GRAPH_COLS + x;
			if (x < GRAPH_COLS - 1) { m_Graph->AddEdge(n, n+1 , false, 0, 1); }
			if (x > 0) { m_Graph->AddEdge(n, n - 1, false, 0, 1); }
			if (y < GRAPH_ROWS - 1) { m_Graph->AddEdge(n, n + GRAPH_COLS, false, 0, 1); }
			if (y > 0) { m_Graph->AddEdge(n, n - GRAPH_COLS, false, 0, 1); }
		}
	}

}

Graph<glm::vec2, float>::Node * GraphVisualiser::ReturnRandomNode()
{
	int randTarID = rand() % (GRAPH_COLS * GRAPH_ROWS);

	return m_Graph->m_Node[randTarID];
}
