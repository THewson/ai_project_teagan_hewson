#include "IBehaviour.h"
#include <Input.h>

void IBehaviourManager::StartUp()
{
	srand(time(NULL));

	OnRadiusEnter([this]() { 
		if (m_currentBehaviour != m_behaviours[0])
		m_currentBehaviour = m_behaviours[0];
	});

	OnRadiusExit([this]() { 
		if(m_currentBehaviour != m_behaviours[1])
		m_currentBehaviour = m_behaviours[1];
	});

	innerRadius = 100.f;
}

void IBehaviourManager::Update(float deltaTime)
{
	if (m_currentBehaviour != nullptr && m_behaviours.size() != 0)
	{
	IBehaviour* add = m_currentBehaviour;
	CheckCircle();
	if (add != m_currentBehaviour)
	{m_currentBehaviour->Transition();}
	}
	
	m_currentBehaviour->Update(deltaTime);
}

void IBehaviourManager::AssignTarget(glm::vec2 tar)
{
	m_currentBehaviour->m_targetPos = tar;
}

// Behaviour Code

IBehaviour::IBehaviour()
{

}

IBehaviour::~IBehaviour()
{

}

void Flee::Update(float deltaTime)
{
	glm::vec2 v = glm::normalize((m_targetPos)-pAgent->m_position) * -100.f;
	//pAgent->m_force = v;
	pAgent->ApplyForce(v);
}

void Flee::Transition()
{

}

void Wander::Update(float deltaTime)
{
	if (pAgent->m_velocity.x <= 5.f && pAgent->m_velocity.y <= 5.f 
		&& pAgent->m_velocity.x >= -5.f && pAgent->m_velocity.y >= -5.f)
	{
		int coinFlip = rand() % 2 + 1;
		
		if (coinFlip == 1)
		pAgent->m_velocity = glm::vec2(rand() % 100, rand() % 100);

		if (coinFlip == 2)
		pAgent->m_velocity = glm::vec2(rand() % -100, rand() % -100);
	}
	circleCentre = pAgent->m_velocity;
	circleCentre = glm::normalize(circleCentre);
	circleCentre *= circleRadius;

	displacement.x = 0;
	displacement.y = 0;
	displacement *= 200;

	int random = rand() % 100 - 50;

	wanderAngle += random + 4.f - (4.f * 0.5f);

	auto length = glm::length(displacement);
	displacement.x = cosf(wanderAngle) * length;
	displacement.y = sinf(wanderAngle) * length;

	glm::vec2 wanderForce = circleCentre + displacement;
	pAgent->ApplyForce(wanderForce * pAgent->m_force);
}

void IBehaviourManager::CheckCircle()
{
	glm::vec2 x = m_currentBehaviour->m_targetPos - lastPosition;
	glm::vec2 l = m_currentBehaviour->m_targetPos - m_currentBehaviour->pAgent->m_position;

	lastDistanceToTarget = glm::length(x);
	distanceToTarget = glm::length(l);

	if (m_OnRadiusEnter && lastDistanceToTarget > innerRadius && distanceToTarget <= innerRadius)
	{m_OnRadiusEnter();}

	if (m_OnRadiusExit && lastDistanceToTarget <= innerRadius && distanceToTarget > innerRadius)
	{m_OnRadiusExit();}

	glm::vec2 dirToTarget = glm::normalize(m_currentBehaviour->m_targetPos - m_currentBehaviour->pAgent->m_position) * m_currentBehaviour->pAgent->m_force;
	m_currentBehaviour->pAgent->ApplyForce(dirToTarget);

	lastPosition = m_currentBehaviour->pAgent->m_position;
}

void IBehaviourManager::OnRadiusEnter(std::function<void()> func){m_OnRadiusEnter = func;}
void IBehaviourManager::OnRadiusExit(std::function<void()> func) {m_OnRadiusExit = func;}

void Seek::Update(float deltaTime)
{
	glm::vec2 v = glm::normalize((m_targetPos) - pAgent->m_position) * 100.f;
	
	pAgent->ApplyForce(v);
}

void Seek::Transition()
{

}

void Controller::Update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();
	pAgent->m_position.x = input->getMouseX();
	pAgent->m_position.y = input->getMouseY();
}

void Controller::Transition()
{

}