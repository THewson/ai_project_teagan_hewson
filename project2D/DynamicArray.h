#pragma once

template <class T>
class DynamicArray
{
public:
	DynamicArray();
	DynamicArray(int startingSize);
	DynamicArray(T startingArray, int numberInUse, int maxCapacity);
	~DynamicArray();
	void CopyArray(T newarray, int size, int currentlyUsed);
	
	void BubbleSort();

	void RemoveElement(int id);
	
	void RemoveFirstElement();
	void RemoveLastElement();

	void AddElement(T elementToAdd);
	void AddElement(T elementToAdd, int posToAdd);

	void ExpandMaxSize();
	void ShrinkToCurrentUsed();

	void EmptyArray();

	T GetElement(int id);
	T* GetArray();
	int GetCurrentlyUsed();
	int GetCapacity();

private:
	T *m_array;
	int m_currentCapacity;
	int m_currentlyUsed;

};

template<class T>
inline DynamicArray<T>::DynamicArray()
{
	m_array = new T[10];
	m_currentCapacity = 10;
	m_currentlyUsed = 0;
}

template<class T>
inline DynamicArray<T>::DynamicArray(T startingArray, int numberInUse, int maxCapacity)
{
	m_currentCapacity = maxCapacity;
	m_currentlyUsed = numberInUse;
	m_array = startingArray;
}

template<class T>
inline DynamicArray<T>::DynamicArray(int startingSize)
{
	m_array = new T[startingSize];
	m_currentCapacity = startingSize;
	m_currentlyUsed = 0;
}

template<class T>
inline DynamicArray<T>::~DynamicArray()
{
}

template<class T>
inline void DynamicArray<T>::CopyArray(T newarray, int size, int currentlyUsed)
{
	m_array = newarray;
	m_currentCapacity = size;
	m_currentlyUsed = currentlyUsed;
}

template<class T>
inline void DynamicArray<T>::BubbleSort()
{
	for (i = 0; i != m_array.size() - 1; i++)
	{
		for (j = m_array.size() - 1; j != i + 1; j++)
		{
 			if (m_array[j] < m_array[i])
			{
				T temp = m_array[i];
				m_array[i] = m_array[j];
				m_array[j] = temp;
			}
		}
	}
	m_array = temp;
}

template<class T>
inline void DynamicArray<T>::RemoveElement(int id)
{
	for (int i = id; i != m_currentCapacity - 1; i++)
	{
		m_array[i] = m_array[i + 1];
	}

	m_currentCapacity -= 1;
}

template<class T>
inline void DynamicArray<T>::RemoveFirstElement()
{
	for (int i = 1; i != m_currentlyUsed - 1; i++)
	{
		m_array[i - 1] = m_array[i];
	}

	m_array = temp;
	m_currentlyUsed = m_currentlyUsed - 1;
}

template<class T>
inline void DynamicArray<T>::RemoveLastElement()
{
	m_currentCapacity -= 1;
}

template<class T>
inline void DynamicArray<T>::AddElement(T elementToAdd)
{
	if (m_currentlyUsed < m_currentCapacity)
	{
		m_array[m_currentlyUsed + 1] = elementToAdd;
	}
	else if (m_currentlyUsed == m_currentCapacity)
	{
		ExpandMaxSize();
		m_array[m_currentlyUsed + 1];
	}

	m_currentlyUsed += 1;
}

template<class T>
inline void DynamicArray<T>::AddElement(T elementToAdd, int posToAdd)
{
	if (m_currentlyUsed < m_currentCapacity)
	{
		for (int i = m_currentlyUsed; i != posToAdd; i--)
		{
			m_array[i] = m_array[i + 1];
		}

		m_array[posToAdd] = elementToAdd;
	}
	else
	{
		ExpandMaxSize();
		AddElement(elementToAdd, posToAdd);
	}
}

template<class T>
inline void DynamicArray<T>::ExpandMaxSize()
{	
	T *newData = new T[m_currentCapacity * 2];
	for (int i = 0; i != m_currentlyUsed; i++)
	{
		newData[i] = m_array[i];
	}
	m_array = newData;
	m_currentCapacity *= 2;
}

template<class T>
inline void DynamicArray<T>::ShrinkToCurrentUsed()
{
	T temp[m_currentlyUsed];

	for (int i = 0; i != m_currentlyUsed; i++)
	{
		temp[i] = m_array[i];
	}

	m_currentCapacity = m_currentlyUsed;
	m_array = temp;
}

template<class T>
inline void DynamicArray<T>::EmptyArray()
{
	m_array = new T[10];
	m_currentCapacity = 10;
	m_currentlyUsed = 0;
}

template<class T>
inline T DynamicArray<T>::GetElement(int id) 
{ 
	if (m_currentCapacity >= id)
	return m_array[id]; 
}

template<class T>
inline T * DynamicArray<T>::GetArray() { return m_array; }

template<class T>
inline int DynamicArray<T>::GetCurrentlyUsed() { return m_currentlyUsed; }

template<class T>
inline int DynamicArray<T>::GetCapacity() { return m_currentCapacity; }
