#pragma once
#include "GameStateMachine.h"
#include "Texture.h"
#include <time.h>

class SplashScreen : public GameState
{
public:

	SplashScreen() {
		m_x = 600;
		m_y = 400;

		m_Logo = new aie::Texture("./textures/logo.png");

		m_startTime = clock();
		m_splashDuration = 2.0f;
	};
	~SplashScreen() {}

	virtual void OnInit() {}
	virtual void OnUpdate(float deltaTime) {
		m_splashDuration -= deltaTime;


		if (m_splashDuration <= 0.0f)
		{
			GameStateManager::m_Manager->PopState();
			GameStateManager::m_Manager->PushState(GameState::MENU);
		}
	}
	virtual void OnDraw(aie::Renderer2D* renderer) {

		renderer->drawSprite(m_Logo, m_x, m_y);
	}

	float m_splashDuration;
	unsigned long m_startTime;
	aie::Texture*	m_Logo;
	int m_x, m_y;
};
