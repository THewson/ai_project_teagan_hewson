#include "stdafx.h"
#include "GameStateMachine.h"

void GameStateManager::RegisterState(int id, GameState * state)
{
	//m_registeredStates[id] = state;
	m_registeredStates.push_back(state);
}

GameStateManager* GameStateManager::m_Manager = NULL;

void GameStateManager::PushState(int id) {
	m_pushedStates.push_back(m_registeredStates[id]);
}

void GameStateManager::PopState() {
	m_popState = true;
}

void GameStateManager::Update(float deltaTime) {

	auto state = GetTopState();
	if (state != nullptr)
	{
		state->OnUpdate(deltaTime);
	}
	

	while (m_popState) {
		m_popState = false;

		// Deactivated Previous Top
		m_stateStack.back()->exit();
		auto temp = m_stateStack.back();
		m_stateStack.pop_back();
		temp->OnPopped();

		// activates the one under the previous top
		// if it exists.
		if (m_stateStack.empty() == false) {
			m_stateStack.back()->exit();
		}
	}

	for (auto pushedState : m_pushedStates) {
		// deactivate the previous top
		if (m_stateStack.empty() == false)
			m_stateStack.back()->exit();

		 //activate new top
		pushedState->OnPushed();
		m_stateStack.push_back(pushedState);
		m_stateStack.back()->enter();
	}

	m_pushedStates.clear();
}

void GameStateManager::Draw(aie::Renderer2D* renderer) {
	
	auto state = GetTopState();
	if (state != nullptr) 
	{
		state->OnDraw(renderer);
	}

}