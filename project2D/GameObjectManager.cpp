#include "GameObjectManager.h"

GameObjectManager::GameObjectManager()
{
	m_AgentNum = 0;
}

GameObjectManager::~GameObjectManager()
{
}

void GameObjectManager::Update(float deltaTime)
{
	for (auto iter : m_AgentList)
	{
 		(*iter).Update(deltaTime);
	}
}

void GameObjectManager::Draw(aie::Renderer2D * renderer)
{
	for (auto iter : m_AgentList)
	{
		renderer->setRenderColour(1.0f, 1.0f, 0.99f, 1.0f);
		renderer->drawCircle((*iter).m_position.x, (*iter).m_position.y, 10, 0.0f);
	}
		renderer->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
}

void GameObjectManager::AddList(std::vector<Agent*>  list, BlackBoard * bb)
{
	for (int i = 0; i < list.size(); i++) {

		m_AgentList.push_back(list.at(i));
		m_AgentNum++;
	}

	for (int i = 0; i != m_AgentNum; i++)
	{
		m_AgentList[i]->AssignBB(bb);
	}
}