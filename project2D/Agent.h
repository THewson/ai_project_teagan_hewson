#pragma once
#include <glm/vec2.hpp>
#include <list>
class IBehaviourManager;
class BlackBoard;

class Agent
{
public:
	Agent();
	~Agent();

	virtual void Update(float deltaTime);
	
	float Magnitude(glm::vec2 thing);
	glm::vec2 Normalise(glm::vec2 thing);
	void CalculateMovement(glm::vec2 * pos);
	void Teleport(float x, float y);
	void AssignBB(BlackBoard * bb);
	virtual void AddBehaviours() = 0;
	void GiveTarget(glm::vec2 tar);
	void GiveBehaviour(int behav);

	void ApplyForce(const glm::vec2 &force);
	
	// How far it moved = displacement.
	glm::vec2 m_force; // Make shit go forward.
	glm::vec2 m_acceleration; //  
	glm::vec2 m_velocity; // Direction + Amount.
	glm::vec2 m_position; // The things position.
	
	float m_maxVelocity; // The fastest a thing can go.
	
	IBehaviourManager * m_IBM;
};
