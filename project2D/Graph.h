#pragma once
#include <vector>
#include <algorithm>
#include <functional>
#include <glm\vec2.hpp>
#include <glm/glm.hpp>


template<typename NODEDATA, typename EDGEDATA>
class Graph {
public:
	Graph() {}
	virtual ~Graph() {
		for (auto iter = m_Node.begin(); iter != m_Node.end(); iter++)
		{
			delete (*iter);
		}
		m_Node.clear();
	}

	struct Node;

	struct Edge {
		Node* target;
		EDGEDATA* m_EdgeData = nullptr;
		float cost;
	};

	struct Node {
		std::vector<Edge*> edges;
		NODEDATA *m_NodeData = nullptr;
		bool visited = false;
		float gScore;
		float hScore;
		float fScore;
		Node * parent;
	};

	// Edge Manipulation
	void AddEdge(int nodeA, int nodeB, bool biDirectional, EDGEDATA * data, float cost)
	{
		AddEdge(m_Node[nodeA], m_Node[nodeB], biDirectional, data, cost);
	}

	void AddEdge(Node* nodeA, Node* nodeB, bool biDirectional, EDGEDATA * data, float cost)
	{
		Edge * newEdge = new Edge;
		newEdge->target = nodeB;
		newEdge->m_EdgeData = data;
		newEdge->cost = cost;

		nodeA->edges.push_back(newEdge);

		if (biDirectional) {
			Edge * newEdge2 = new Edge;
			newEdge2->target = nodeA;
			newEdge2->m_EdgeData = data;
			newEdge2->cost = cost;
			nodeB->edges.push_back(newEdge2);
		}
	}

	void RemoveEdge(int verNo, int verEdge)
	{
		m_Node[verNo]->edges[verEdge]->target = nullptr;
		delete m_Node[verNo]->edges[verEdge];
	}
	
	void ReassignEdgeTarget(int verNo, int verEdge, int newVerCon)
	{
		m_Node[verNo]->edges[verEdge]->target = m_Node[newVerCon];
	}

	// Node Manipulation
	void AddNode()
	{
		m_Node.push_back(new Node);
	}
	void AddNode(NODEDATA * node)
	{
		Node* n = new Node;
		n->m_NodeData = node;
		m_Node.push_back(n);
	}

	void GetNearbyNodes(Node* pos, float radius, std::vector<Node *> nearbyNodes) {
		for (auto iter = m_Node.begin(); iter != m_Node.end(); iter++)
		{
			Node* node = (*iter);
			float x = node->m_NodeData - pos->m_NodeData;
			float dist = glm::length(x);
			
			if (dist < radius) {
				nearbyNodes.push_back(node);
			}
		}
	}


	const std::vector<Node*> & GetNodes() const { return m_Node; }

	std::vector<Node*> m_Node;
	
};