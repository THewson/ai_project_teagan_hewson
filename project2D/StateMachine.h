#pragma once
#include <vector>
class GameObject;
class State;

class Condition {
public:
	virtual ~Condition() {}
	virtual bool Test(GameObject* go) const = 0;
};

class Transition final {
public:
	Transition(Condition* condition, State* target) {
		m_condition = condition;
		m_state = target;
	}
	~Transition() { delete m_condition; }

	bool IsConditionMet(GameObject* gameObject) const {
		return m_condition->Test(gameObject);
	}

	State* GetTargetState() const { return m_state; }

private:
	State* m_state;
	Condition* m_condition;
};

class State {
public:
	virtual ~State()
	{
		for (auto t : m_transitions)
			delete t;
	}
	virtual void OnEnter(GameObject* object) {}

	virtual void OnExit(GameObject* object) {}

	virtual void OnUpdate(GameObject* object, float dt) = 0;

	void AddTransition(Transition* transition) { m_transitions.push_back(transition); }

	State* GetNextState(GameObject* gameObject)
	{
		for (auto transition : m_transitions) {
			if (transition->IsConditionMet(gameObject))
				return transition->GetTargetState();
		} //no state change 
		return nullptr; } 

protected: 
	std::vector<Transition*> m_transitions; 
};

class FiniteStateMachine {
public:
	FiniteStateMachine(int stateCount) { m_states.resize(stateCount); }
	virtual ~FiniteStateMachine()
	{
		for (auto state : m_states)
			delete state;
	}
	void AddState(int id, State* newState) { m_states[id] = newState; }
	void ForceState(int id) { m_currentState = m_states[id]; }
	void Update(GameObject* gameObject, float deltaTime)
	{
		if (m_currentState != nullptr) {
			m_currentState -> OnUpdate(gameObject, deltaTime);
			State* next = m_currentState->GetNextState(gameObject);
			if (next != m_currentState && next != nullptr) {
				m_currentState->OnExit(gameObject);
				m_currentState = next;
				m_currentState->OnEnter(gameObject);
			}
		}
	}

protected:
	State* m_currentState = nullptr;
	std::vector<State*> m_states;
};


