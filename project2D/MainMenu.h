#pragma once
#include "GameStateMachine.h"
#include "Vector2.h"
#include <Texture.h>

class MainMenu : public GameState
{
public:
	MainMenu();
	~MainMenu();

	virtual void OnInit();
	virtual void OnUpdate(float deltaTime);

	virtual void OnDraw(aie::Renderer2D* renderer);

	aie::Texture* m_stTex;
	aie::Texture* m_exTex;
	Vector2 m_exPoint;
	Vector2 m_stPoint;
	int m_stWidth, m_stHeight;
	int m_exWidth, m_exHeight;

};
