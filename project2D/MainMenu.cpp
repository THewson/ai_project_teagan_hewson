#include "MainMenu.h"
#include "Input.h"

MainMenu::MainMenu()
{
	m_exTex = new aie::Texture("./textures/exitButton.png");
	m_stTex = new aie::Texture("./textures/startButton.png");

	m_exPoint.m_posX = 300;
	m_exPoint.m_posY = 200;
	m_exWidth = 163;
	m_exHeight = 42;

	m_stPoint.m_posX = 300;
	m_stPoint.m_posY = 500;
	m_stWidth = 163;
	m_stHeight = 42;
}

MainMenu::~MainMenu()
{

}

void MainMenu::OnInit()
{
}

void MainMenu::OnUpdate(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();
	//&&(pointX > x - (width / 2)
	//&& pointX < x + (width / 2) 
	//&& pointY > y - (height / 2) 
	//&& pointY < y + (height / 2)

	if (input->getMouseX() >= (m_exPoint.m_posX - (m_exWidth / 2)) &&
		input->getMouseX() <= (m_exPoint.m_posX + (m_exHeight / 2)) &&
		input->getMouseY() >= (m_exPoint.m_posY - (m_exWidth / 2)) &&
		input->getMouseY() <= (m_exPoint.m_posY + (m_exHeight / 2)))
	{
		if (input->wasMouseButtonPressed(0))
		{
			GameStateManager::m_Manager->PopState();
			GameStateManager::m_Manager->PushState(GameState::EXIT);
		}
	}

	if (input->getMouseX() >= (m_stPoint.m_posX - (m_stWidth / 2)) &&
		input->getMouseX() <= (m_stPoint.m_posX + (m_stHeight / 2)) &&
		input->getMouseY() >= (m_stPoint.m_posY - (m_stWidth / 2)) &&
		input->getMouseY() <= (m_stPoint.m_posY + (m_stHeight / 2)))
	{
		if (input->wasMouseButtonPressed(0))
		{
			GameStateManager::m_Manager->PopState();
			GameStateManager::m_Manager->PushState(GameState::GAME);
		}
	}

}

void MainMenu::OnDraw(aie::Renderer2D * renderer)
{
	//renderer->drawSprite(m_exTex, 200, 200, 0, 0);
	renderer->drawSprite(m_exTex, m_exPoint.m_posX, m_exPoint.m_posY);
	renderer->drawSprite(m_stTex, m_stPoint.m_posX, m_stPoint.m_posY);
}

