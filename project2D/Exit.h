#pragma once
#include "GameStateMachine.h"

class ExitState : public GameState
{
public:

	ExitState() {
		m_x = 600;
		m_y = 400;

		m_Logo = new aie::Texture("./textures/exit.png");

		m_startTime = clock();
		m_splashDuration = 5.0f;
	};
	~ExitState() {}

	virtual void OnInit() {}
	virtual void OnUpdate(float deltaTime) {
		m_splashDuration -= deltaTime;


		if (m_splashDuration <= 0.0f)
		{
			GameStateManager::m_Manager->PopState();
		}
	}
	virtual void OnDraw(aie::Renderer2D* renderer) {

		renderer->drawSprite(m_Logo, m_x, m_y);
	}

	float m_splashDuration;
	unsigned long m_startTime;
	aie::Texture*	m_Logo;
	int m_x, m_y;
};

