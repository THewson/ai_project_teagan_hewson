#pragma once
#include "Agent.h"
#include "Renderer2D.h"
#include <vector>

#include "Hunted.h"
#include "Persuer.h"
class BlackBoard;

class GameObjectManager
{
public:
	GameObjectManager();
	~GameObjectManager();
	void Update(float deltaTime);
	void Draw(aie::Renderer2D * renderer);

	void AddList(std::vector<Agent*> list, BlackBoard * bb);

	std::vector<Agent*> m_AgentList;
	int m_AgentNum;
};

