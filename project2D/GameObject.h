#pragma once
#pragma once
#include "Matrix2.h"
#include "Vector2.h"
#include "Matrix3.h"
#include <vector>

#include "Application2D.h"

class Entity {
public:
	Entity() : m_parent(nullptr) {}
	virtual ~Entity() {
		for (auto child : m_children)
			delete child;
	}

	void AddChild(Entity* child) {
		if (child->m_parent == nullptr)
		{
			child->m_parent = this;
			m_children.push_back(child);
		}
	}

	virtual void Update(float deltaTime, Vector3 newPos) {
		if (m_parent != nullptr)
		{
			m_globalTransform = m_parent->m_globalTransform * m_localTransform;
		}
		else {
			m_localTransform.m_vector[2] = newPos;
			m_globalTransform = m_localTransform;
		}

		for (auto child : m_children)
			child->Update(deltaTime, newPos);
	}

	virtual void Draw(aie::Renderer2D* renderer) {
		for (auto child : m_children)
			child->Draw(renderer);
	}

	Matrix3 m_globalTransform;
	Matrix3 m_localTransform;

	Entity* m_parent;
	std::vector<Entity*> m_children;
};

class GameObject : public Entity {
public:
	GameObject();
	GameObject(aie::Texture* texture)
		: m_texture(texture) {}
	virtual ~GameObject() {}

	virtual void Draw(aie::Renderer2D* renderer);

	Matrix3 getGlobalTransform() { return m_globalTransform; }
	Matrix3 getLocalTransform() { return m_localTransform; }

	Matrix2 m_rotation;
	aie::Texture* m_texture;
};

